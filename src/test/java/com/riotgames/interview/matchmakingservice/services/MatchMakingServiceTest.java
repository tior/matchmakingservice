package com.riotgames.interview.matchmakingservice.services;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import com.riotgames.interview.matchmakingservice.dtos.MatchedPlayersMessageDto;
import com.riotgames.interview.matchmakingservice.dtos.QueueRequestMessageDto;
import com.riotgames.interview.matchmakingservice.dtos.QueueStatusResponsePayload;
import com.riotgames.interview.matchmakingservice.enums.GameType;
import com.riotgames.interview.matchmakingservice.enums.QueueStatus;
import com.riotgames.interview.matchmakingservice.enums.Rank;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.SenderResult;
import reactor.test.StepVerifier;

public class MatchMakingServiceTest {

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private ReactiveKafkaProducerTemplate<String, MatchedPlayersMessageDto>
      matchedPlayersMessageDtoProducer;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private ReactiveKafkaProducerTemplate<String, QueueRequestMessageDto>
      queueRequestMessageDtoProducer;

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private QueueServiceService queueServiceService;

  private MatchMakingService matchMakingService;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
    matchMakingService =
        new MatchMakingService(
            matchedPlayersMessageDtoProducer, queueRequestMessageDtoProducer, queueServiceService);
  }

  @Test
  public void createMatchPlayerCancelledRequestShouldRequeuePlayersAndReturnEmptyMono() {
    List<QueueRequestMessageDto> requestMessageDtos =
        Arrays.asList(
            QueueRequestMessageDto.builder().queueId("1").userId("user1").build(),
            QueueRequestMessageDto.builder().queueId("2").userId("user2").build(),
            QueueRequestMessageDto.builder().queueId("3").userId("user3").build());
    String requeueTopicName = "requeueTopic";

    when(queueServiceService.getQueueStatus("1"))
        .thenReturn(
            Mono.just(
                QueueStatusResponsePayload.builder().queueStatus(QueueStatus.FINDING).build()));
    when(queueServiceService.getQueueStatus("2"))
        .thenReturn(
            Mono.just(
                QueueStatusResponsePayload.builder().queueStatus(QueueStatus.CANCELLED).build()));
    when(queueServiceService.getQueueStatus("3"))
        .thenReturn(
            Mono.just(
                QueueStatusResponsePayload.builder().queueStatus(QueueStatus.FINDING).build()));

    StepVerifier.create(matchMakingService.createMatch(requestMessageDtos, requeueTopicName))
        .verifyComplete();

    verify(queueRequestMessageDtoProducer, times(1)).send(eq(requeueTopicName), eq("user1"), any());
    verify(queueRequestMessageDtoProducer, never()).send(eq(requeueTopicName), eq("user2"), any());
    verify(queueRequestMessageDtoProducer, times(1)).send(eq(requeueTopicName), eq("user3"), any());
    verify(matchedPlayersMessageDtoProducer, never()).send(any(), any(), any());
  }

  @Test
  public void createMatchAllPlayersAvailableShouldCreateMatchAndSendToReadyQ() {
    List<QueueRequestMessageDto> requestMessageDtos =
        Arrays.asList(
            QueueRequestMessageDto.builder()
                .queueId("1")
                .userId("user1")
                .rank(Rank.BRONZE)
                .gameType(GameType._1V1)
                .build(),
            QueueRequestMessageDto.builder()
                .queueId("2")
                .userId("user2")
                .rank(Rank.SILVER)
                .gameType(GameType._1V1)
                .build(),
            QueueRequestMessageDto.builder()
                .queueId("3")
                .userId("user3")
                .rank(Rank.GOLD)
                .gameType(GameType._1V1)
                .build());
    String requeueTopicName = "requeueTopic";
    MatchedPlayersMessageDto matchedPlayersMessageDto =
        MatchedPlayersMessageDto.builder()
            .matchId("match1")
            .team1Players(
                Collections.singletonList(
                    QueueRequestMessageDto.builder()
                        .queueId("1")
                        .userId("user1")
                        .rank(Rank.BRONZE)
                        .gameType(GameType._1V1)
                        .build()))
            .team2Players(
                Arrays.asList(
                    QueueRequestMessageDto.builder()
                        .queueId("2")
                        .userId("user2")
                        .rank(Rank.SILVER)
                        .gameType(GameType._1V1)
                        .build(),
                    QueueRequestMessageDto.builder()
                        .queueId("3")
                        .userId("user3")
                        .rank(Rank.GOLD)
                        .gameType(GameType._1V1)
                        .build()))
            .rank(Rank.BRONZE)
            .gameType(GameType._1V1)
            .createdDateTime(OffsetDateTime.now())
            .build();

    when(queueServiceService.getQueueStatus("1"))
        .thenReturn(
            Mono.just(
                QueueStatusResponsePayload.builder().queueStatus(QueueStatus.FINDING).build()));
    when(queueServiceService.getQueueStatus("2"))
        .thenReturn(
            Mono.just(
                QueueStatusResponsePayload.builder().queueStatus(QueueStatus.FINDING).build()));
    when(queueServiceService.getQueueStatus("3"))
        .thenReturn(
            Mono.just(
                QueueStatusResponsePayload.builder().queueStatus(QueueStatus.FINDING).build()));

    SenderResult<Void> senderResultMock = Mockito.mock(SenderResult.class);
    when(matchedPlayersMessageDtoProducer.send(any(), any(), any()))
        .thenReturn(Mono.just(senderResultMock));

    StepVerifier.create(matchMakingService.createMatch(requestMessageDtos, requeueTopicName))
        .verifyComplete();

    verify(queueRequestMessageDtoProducer, never()).send(any(), any(), any());
    verify(matchedPlayersMessageDtoProducer, times(1)).send(any(), any(), any());
  }
}
