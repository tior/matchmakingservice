package com.riotgames.interview.matchmakingservice.dtos;

import com.riotgames.interview.matchmakingservice.enums.GameType;
import com.riotgames.interview.matchmakingservice.enums.Rank;
import java.time.OffsetDateTime;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.jackson.Jacksonized;

@Builder
@Getter
@Jacksonized
@ToString
public class QueueRequestMessageDto {
  private String userId;
  private String queueId;
  private Rank rank;
  private GameType gameType;
  private OffsetDateTime createdDateTime;
}
