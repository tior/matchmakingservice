package com.riotgames.interview.matchmakingservice.dtos;

import com.riotgames.interview.matchmakingservice.enums.GameType;
import com.riotgames.interview.matchmakingservice.enums.Rank;
import java.time.OffsetDateTime;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Builder
@Getter
@Jacksonized
public class MatchedPlayersMessageDto {
  private String matchId;
  private List<QueueRequestMessageDto> team1Players;
  private List<QueueRequestMessageDto> team2Players;
  private Rank rank;
  private GameType gameType;
  private OffsetDateTime createdDateTime;
}
