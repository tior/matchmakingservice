package com.riotgames.interview.matchmakingservice.dtos;

import com.riotgames.interview.matchmakingservice.enums.GameType;
import com.riotgames.interview.matchmakingservice.enums.QueueStatus;
import com.riotgames.interview.matchmakingservice.enums.Rank;
import java.time.OffsetDateTime;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;
import lombok.extern.slf4j.Slf4j;

@Builder
@Getter
@Slf4j
@Jacksonized
public class QueueStatusResponsePayload {
  private String userId;
  private String queueId;
  private Rank rank;
  private GameType gameType;
  private OffsetDateTime createdDateTime;
  private OffsetDateTime updatedDateTime;
  private QueueStatus queueStatus;
  private String matchId;
}
