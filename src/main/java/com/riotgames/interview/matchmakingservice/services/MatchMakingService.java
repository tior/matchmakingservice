package com.riotgames.interview.matchmakingservice.services;

import com.riotgames.interview.matchmakingservice.dtos.MatchedPlayersMessageDto;
import com.riotgames.interview.matchmakingservice.dtos.QueueRequestMessageDto;
import com.riotgames.interview.matchmakingservice.dtos.QueueStatusResponsePayload;
import com.riotgames.interview.matchmakingservice.enums.GameType;
import com.riotgames.interview.matchmakingservice.enums.QueueStatus;
import com.riotgames.interview.matchmakingservice.enums.Rank;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class MatchMakingService {

  private final ReactiveKafkaProducerTemplate<String, MatchedPlayersMessageDto>
      matchedPlayersMessageDtoProducer;
  private final ReactiveKafkaProducerTemplate<String, QueueRequestMessageDto>
      queueRequestMessageDtoProducer;
  private final QueueServiceService queueServiceService;

  @Value("${READY_Q_TOPIC}")
  private String READY_Q_TOPIC;

  public MatchMakingService(
      ReactiveKafkaProducerTemplate<String, MatchedPlayersMessageDto>
          matchedPlayersMessageDtoProducer,
      ReactiveKafkaProducerTemplate<String, QueueRequestMessageDto> queueRequestMessageDtoProducer,
      QueueServiceService queueServiceService) {
    this.matchedPlayersMessageDtoProducer = matchedPlayersMessageDtoProducer;
    this.queueRequestMessageDtoProducer = queueRequestMessageDtoProducer;
    this.queueServiceService = queueServiceService;
  }

  public Mono<Void> createMatch(
      List<QueueRequestMessageDto> requestMessageDtos, String requeueTopicName) {
    int requestMessageDtosSize = requestMessageDtos.size();
    log.info("Creating match for {} with size {}", requestMessageDtos, requestMessageDtosSize);

    return checkIfAnyPlayerCancelledQueue(requestMessageDtos)
        .flatMap(
            filteredList -> {
              log.info(
                  "Filtered list {} vs requestMessageDtos {}",
                  filteredList.size(),
                  requestMessageDtosSize);
              if (filteredList.size() != requestMessageDtosSize) {
                return Flux.fromIterable(filteredList)
                    .map(
                        queueRequestMessageDto ->
                            requeuePlayer(queueRequestMessageDto, requeueTopicName)
                                .doOnSuccess(
                                    unused ->
                                        log.info(
                                            "Successfully re-queued {}",
                                            queueRequestMessageDto.getUserId())))
                    .then(Mono.empty());
              } else {
                return Mono.just(filteredList);
              }
            })
        .switchIfEmpty(
            Mono.defer(
                () -> {
                  log.error(
                      "The list of players were requeued to {} because one or more players cancelled their request",
                      requeueTopicName);
                  return Mono.empty();
                }))
        .flatMap(this::segregatePlayers)
        .flatMap(this::sendToReadyQ)
        .then();
  }

  private Mono<Void> requeuePlayer(
      QueueRequestMessageDto queueRequestMessageDto, String topicName) {
    return queueRequestMessageDtoProducer
        .send(topicName, queueRequestMessageDto.getUserId(), queueRequestMessageDto)
        .then();
  }

  private Mono<Void> sendToReadyQ(MatchedPlayersMessageDto matchedPlayersMessageDto) {
    return matchedPlayersMessageDtoProducer
        .send(READY_Q_TOPIC, matchedPlayersMessageDto.getMatchId(), matchedPlayersMessageDto)
        .then();
  }

  private Mono<Boolean> didPlayerCancelRequest(String queueId) {
    return queueServiceService
        .getQueueStatus(queueId)
        .map(QueueStatusResponsePayload::getQueueStatus)
        .map(
            queueStatus ->
                queueStatus == QueueStatus.MATCHED || queueStatus == QueueStatus.CANCELLED)
        .doFirst(() -> log.info("Did Player Cancel queue Request for queueId {}?", queueId))
        .doOnSuccess(
            aBoolean ->
                log.info("Player cancellation request for queueId {} is {}", queueId, aBoolean));
  }

  private Mono<List<QueueRequestMessageDto>> checkIfAnyPlayerCancelledQueue(
      List<QueueRequestMessageDto> queueRequestMessageDtos) {
    return Flux.fromIterable(queueRequestMessageDtos)
        .flatMap(
            queueRequestMessageDto ->
                didPlayerCancelRequest(queueRequestMessageDto.getQueueId())
                    .flatMap(
                        cancelled -> {
                          if (!cancelled) {
                            return Mono.just(queueRequestMessageDto);
                          } else {
                            return Mono.empty();
                          }
                        }))
        .collectList()
        .doOnSuccess(list -> log.info("Collected list {} has size {}", list, list.size()));
  }

  private Mono<MatchedPlayersMessageDto> segregatePlayers(
      List<QueueRequestMessageDto> queueRequestMessageDtos) {
    int size = queueRequestMessageDtos.size();
    int mid = size / 2;
    String matchId = UUID.randomUUID().toString();

    Rank rank = queueRequestMessageDtos.stream().findFirst().get().getRank();
    GameType gameType = queueRequestMessageDtos.stream().findFirst().get().getGameType();

    return Mono.just(
            MatchedPlayersMessageDto.builder()
                .team1Players(queueRequestMessageDtos.subList(0, mid))
                .team2Players(queueRequestMessageDtos.subList(mid, size))
                .rank(rank)
                .gameType(gameType)
                .matchId(matchId)
                .createdDateTime(OffsetDateTime.now())
                .build())
        .doFirst(
            () ->
                log.info(
                    "Segregating players into a match with matchId {} for players {} of size {} and gameType {}",
                    matchId,
                    queueRequestMessageDtos,
                    queueRequestMessageDtos.size(),
                    gameType))
        .doOnSuccess(
            matchedPlayersMessageDto ->
                log.info(
                    "Successfully segregated players for matchId {} of size {} and gameType {}",
                    matchedPlayersMessageDto.getMatchId(),
                    queueRequestMessageDtos.size(),
                    matchedPlayersMessageDto.getGameType()));
  }
}
