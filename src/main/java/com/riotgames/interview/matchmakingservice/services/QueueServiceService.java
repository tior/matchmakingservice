package com.riotgames.interview.matchmakingservice.services;

import com.riotgames.interview.matchmakingservice.dtos.QueueStatusResponsePayload;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class QueueServiceService {
  private WebClient webClient;

  public QueueServiceService(WebClient webClient) {
    this.webClient = webClient;
  }

  public Mono<QueueStatusResponsePayload> getQueueStatus(String queueId) {
    return webClient
        .get()
        .uri("/queues/" + queueId)
        .retrieve()
        .bodyToMono(QueueStatusResponsePayload.class)
        .doFirst(() -> log.info("Getting queue status for queueId {}", queueId))
        .doOnSuccess(
            queueStatusResponsePayload ->
                log.info(
                    "Received queue status {} for queueId {}",
                    queueStatusResponsePayload,
                    queueId));
  }
}
