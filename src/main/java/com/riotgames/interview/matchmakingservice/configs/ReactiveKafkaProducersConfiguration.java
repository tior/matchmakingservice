package com.riotgames.interview.matchmakingservice.configs;

import com.riotgames.interview.matchmakingservice.dtos.MatchedPlayersMessageDto;
import com.riotgames.interview.matchmakingservice.dtos.QueueRequestMessageDto;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import reactor.kafka.sender.SenderOptions;

@Configuration
@Slf4j
public class ReactiveKafkaProducersConfiguration {

  @Bean("queueRequestMessageDtoProducer")
  public ReactiveKafkaProducerTemplate<String, QueueRequestMessageDto>
      queueRequestMessageDtoReactiveKafkaProducerTemplate(KafkaProperties properties) {
    Map<String, Object> props = properties.buildProducerProperties();
    return new ReactiveKafkaProducerTemplate<>(SenderOptions.create(props));
  }

  @Bean("matchedPlayersMessageDtoProducer")
  public ReactiveKafkaProducerTemplate<String, MatchedPlayersMessageDto>
      matchedPlayersMessageDtoReactiveKafkaProducerTemplate(KafkaProperties properties) {
    Map<String, Object> props = properties.buildProducerProperties();
    return new ReactiveKafkaProducerTemplate<>(SenderOptions.create(props));
  }
}
