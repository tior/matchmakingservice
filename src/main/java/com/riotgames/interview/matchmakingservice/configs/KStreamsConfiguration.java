package com.riotgames.interview.matchmakingservice.configs;

import static org.apache.kafka.streams.StreamsConfig.*;

import java.util.Map;
import org.apache.kafka.common.serialization.Serdes;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.kafka.config.KafkaStreamsConfiguration;

@Configuration
@EnableKafkaStreams
public class KStreamsConfiguration {
  @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
  public KafkaStreamsConfiguration kafkaStreamConfiguration(KafkaProperties kafkaProperties) {
    Map<String, Object> kafkaProps = kafkaProperties.buildStreamsProperties();
    kafkaProps.put(DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
    kafkaProps.put(DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
    return new KafkaStreamsConfiguration(kafkaProps);
  }
}
