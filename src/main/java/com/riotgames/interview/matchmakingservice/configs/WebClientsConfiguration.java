package com.riotgames.interview.matchmakingservice.configs;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@Slf4j
public class WebClientsConfiguration {

  @Bean("queueServiceWebClient")
  public WebClient webClient(@Value("${queueservice.baseUrl}") String queueServiceBaseUrl) {
    return WebClient.create(queueServiceBaseUrl);
  }
}
