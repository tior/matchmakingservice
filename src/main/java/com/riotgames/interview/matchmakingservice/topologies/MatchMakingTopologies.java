package com.riotgames.interview.matchmakingservice.topologies;

import com.fasterxml.jackson.core.type.TypeReference;
import com.riotgames.interview.matchmakingservice.dtos.QueueRequestMessageDto;
import com.riotgames.interview.matchmakingservice.services.MatchMakingService;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.function.Function;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.kafka.support.serializer.JsonSerializer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.SenderResult;

@Configuration
@Slf4j
public class MatchMakingTopologies {

  @Value("#{${MATCH_MAKING_TOPIC_PLAYERSIZE_MAP}}")
  private Map<String, Integer> matchMakingTopicPlayersizeMap;

  @Bean
  public Topology createMatchMakingStreams(
      StreamsBuilder streamsBuilder,
      MatchMakingService matchMakingService,
      ReactiveKafkaProducerTemplate<String, QueueRequestMessageDto> reactiveKafkaProducerTemplate) {
    log.info("Creating matchmaking streams for {}", matchMakingTopicPlayersizeMap);
    JsonSerde<QueueRequestMessageDto> jsonSerde = new JsonSerde<>(QueueRequestMessageDto.class);
    jsonSerde.ignoreTypeHeaders();

    matchMakingTopicPlayersizeMap.forEach(
        (topicName, playerSize) -> {
          KStream<String, QueueRequestMessageDto> kStream =
              streamsBuilder.stream(topicName, Consumed.with(Serdes.String(), jsonSerde));

          KStream<String, QueueRequestMessageDto> processedStream = processStream(kStream);
          processedStream
              .groupByKey()
              .windowedBy(
                  TimeWindows.ofSizeAndGrace(Duration.ofSeconds(30), Duration.ofSeconds(30)))
              .aggregate(
                  HashMap<String, QueueRequestMessageDto>::new,
                  (key, player, players) -> {
                    players.put(player.getUserId(), player);
                    log.info("adding {}, queue size: {}", player, players.size());
                    if (players.size() == playerSize) {
                      matchMakingService
                          .createMatch(players.values().stream().toList(), topicName)
                          .subscribe();
                      players.clear();
                    }
                    return players;
                  },
                  Materialized.with(
                      Serdes.String(),
                      Serdes.serdeFrom(
                          new JsonSerializer<>(),
                          new JsonDeserializer<>(
                              new TypeReference<
                                  HashMap<
                                      String, QueueRequestMessageDto>>() {})))) // if we don't use
              // TypeReference here we get
              // serialization exception
              // when requeue players
              .toStream()
              .foreach(
                  (windowedKey, list) -> {
                    processMatchList(
                        list.values().stream().toList(),
                        playerSize,
                        topicName,
                        reactiveKafkaProducerTemplate);
                    list.clear();
                  });
        });

    return streamsBuilder.build();
  }

  private KStream<String, QueueRequestMessageDto> processStream(
      KStream<String, QueueRequestMessageDto> stream) {
    return stream.selectKey((key, player) -> player.getGameType().toString());
  }

  private void processMatchList(
      List<QueueRequestMessageDto> list,
      int playerSize,
      String topicName,
      ReactiveKafkaProducerTemplate<String, QueueRequestMessageDto> reactiveKafkaProducerTemplate) {
    if (list.size() < playerSize && list.size() > 0) {
      log.error("No match found in 30s window, re-queuing players {}", list);
      Flux.fromIterable(list)
          .filter(
              queueRequestMessageDto ->
                  OffsetDateTime.now()
                      .isBefore(
                          queueRequestMessageDto.getCreatedDateTime().plus(Duration.ofMinutes(5))))
          .doOnDiscard(
              QueueRequestMessageDto.class,
              queueRequestMessageDto ->
                  log.error(
                      "User {} with queueId {} has been queueing since {}, which is more than 5mins. Dropping",
                      queueRequestMessageDto.getUserId(),
                      queueRequestMessageDto.getQueueId(),
                      queueRequestMessageDto.getCreatedDateTime()))
          .flatMap(
              queueRequestMessageDto ->
                  requeuePlayers(reactiveKafkaProducerTemplate, topicName, queueRequestMessageDto))
          .subscribe();
    }
  }

  private Mono<SenderResult<Void>> requeuePlayers(
      ReactiveKafkaProducerTemplate<String, QueueRequestMessageDto> reactiveKafkaProducerTemplate,
      String topicName,
      QueueRequestMessageDto queueRequestMessageDto) {
    return reactiveKafkaProducerTemplate.send(topicName, queueRequestMessageDto);
  }

  private static Function<QueueRequestMessageDto, Publisher<? extends SenderResult<Void>>>
      requeuePlayers(
          ReactiveKafkaProducerTemplate<String, QueueRequestMessageDto>
              reactiveKafkaProducerTemplate,
          String topicName) {
    return queueRequestMessageDto ->
        reactiveKafkaProducerTemplate
            .send(topicName, queueRequestMessageDto.getUserId(), queueRequestMessageDto)
            .doOnError(
                throwable ->
                    log.error(
                        "Failed to queue player {} back to {}",
                        queueRequestMessageDto.getUserId(),
                        topicName,
                        throwable))
            .doOnSuccess(
                voidSenderResult ->
                    log.info(
                        "Successfully requeued player {} to {}",
                        queueRequestMessageDto.getUserId(),
                        voidSenderResult.recordMetadata().topic()));
  }
}
