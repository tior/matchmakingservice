package com.riotgames.interview.matchmakingservice.enums;

public enum QueueStatus {
  CANCELLED,
  FINDING,
  MATCHED
}
