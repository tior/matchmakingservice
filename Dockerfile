FROM openjdk:17-jdk-slim-buster
WORKDIR /app
COPY target/matchmakingservice-*-SNAPSHOT.jar myapp.jar
CMD java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:8092 -jar myapp.jar

